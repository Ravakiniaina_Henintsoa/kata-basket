# kata-basket
L'application permet de visualiser une liste de produits et d'ajouter ces produits à un panier. 

# Fonctionnalités
Affichage des Produits
Ajout au un panier en spécifiant la quantité désirée
Modification du panier
Suppression panier
Retirez les articles du panier si la date dépasse du 24h

# Prérequis
Avant de commencer, assurez-vous d'avoir les éléments suivants installés sur votre machine :
	Node.js
	npm (Node Package Manager)
	java 21

## Installation
```
cd existing_repo
git remote add origin https://gitlab.com/Ravakiniaina_Henintsoa/kata-basket.git
git branch -M main
git push -uf origin main
```

## Build
Back-end:
```
cd existing_repo\demo
./gradlew bootRun
```
Front-end:
```
cd existing_repo\front
npm install
npm start
```

L'application sera accessible à l'adresse http://localhost:3000/ dans votre navigateur.

#Auteur
Ravaka