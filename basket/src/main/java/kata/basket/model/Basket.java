package kata.basket.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Basket {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @OneToMany(mappedBy = "basket", cascade = CascadeType.ALL)
    private List<BasketItem> items = new ArrayList<>();
    @Column(name = "created_date", nullable = false)
    private LocalDateTime createddate;
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;
    public Basket(User user) {
        this.user = user;
    }
}
