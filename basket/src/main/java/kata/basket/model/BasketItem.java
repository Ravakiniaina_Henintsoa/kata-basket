package kata.basket.model;

import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "basket_item")
@Data
@NoArgsConstructor
public class BasketItem {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne
    @JoinColumn(name = "product_id")
    private Product product;
    @ManyToOne
    @JoinColumn(name = "basket_id", nullable = false)
    private Basket basket;
    @Column(name = "quantity", nullable = false)
    private int quantity;
    public BasketItem(Product product, Basket basket, int quantity) {
        this.product = product;
        this.basket = basket;
        this.quantity = quantity;
    }
    public BasketItem(Long id, Product product, int quantity) {
        this.id = id;
        this.product = product;
        this.quantity = quantity;
    }
}
