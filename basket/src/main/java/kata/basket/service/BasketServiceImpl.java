package kata.basket.service;

import kata.basket.dto.BasketDTO;
import kata.basket.dto.BasketItemDTO;
import kata.basket.dto.ProductDTO;
import kata.basket.exception.BasketNotFoundException;
import kata.basket.exception.ProductNotFoundException;
import kata.basket.exception.ProductQuantityAvailableException;
import kata.basket.model.Basket;
import kata.basket.model.BasketItem;
import kata.basket.model.Product;
import kata.basket.model.User;
import kata.basket.repository.BasketRepository;
import kata.basket.repository.ProductRepository;
import kata.basket.repository.UserRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class BasketServiceImpl implements BasketService {
    private final BasketRepository basketRepository;
    private final UserRepository userRepository;
    private final ProductRepository productRepository;
    private final ProductService productService;

    private final ModelMapper modelMapper;

    public BasketServiceImpl(BasketRepository basketRepository, ProductRepository productRepository, UserRepository userRepository, ProductService productService, ModelMapper modelMapper) {
        this.basketRepository       = basketRepository;
        this.productRepository      = productRepository;
        this.userRepository         = userRepository;
        this.productService         = productService;
        this.modelMapper    = modelMapper;
    }
    @Override
    public Basket create() {
        Basket basket = new Basket();
        basket.setCreateddate(LocalDateTime.now());
        basket.setUser(new User(1L, "Ravaka", "Henintsoa", "ravaka", "test"));
        return basketRepository.save(basket);
    }
    @Override
    public BasketDTO findById(Long id) {
        Basket basket = basketRepository.findById(id).orElseThrow(() -> new BasketNotFoundException(id));
        return this.modelMapper.map(basket, BasketDTO.class);
    }
    @Override
    public BasketDTO addItemToBasket(Long productId, int quantity) {
        BasketDTO basketDTO = this.getCurrent();
        Basket basket = this.modelMapper.map(basketDTO, Basket.class);
        Product product = productRepository.findById(productId).orElseThrow(() -> new ProductNotFoundException(productId));
        if (product.getQuantityAvailable() == 0)
            throw new ProductQuantityAvailableException(product.getId());
        BasketItem basketItem = new BasketItem(product, basket, quantity);
        basket.getItems().add(basketItem);
        productService.decrementQuantityAvailable(product, quantity);
        return this.modelMapper.map(basketRepository.save(basket), BasketDTO.class);
    }
    @Override
    public void removeItemFromBasketAuto() {
        List<Basket> lBasket = basketRepository.findAll();
        for (Basket basket: lBasket) {
            LocalDateTime expire = basket.getCreateddate().plusHours(24);
            if (LocalDateTime.now().compareTo(expire) > 0)
                basketRepository.delete(basket);
        }
    }

    @Override
    public BasketDTO getCurrent() {
        List<Basket> lBasket = basketRepository.findAll();
        Basket basket = lBasket.stream().findFirst().orElse(this.create());
        BasketDTO basketDTO                 = this.modelMapper.map(basket, BasketDTO.class);
        List<BasketItemDTO> lBasketItemDTO  = new ArrayList<>();
        for (BasketItem basketItem : basket.getItems()) {
            BasketItemDTO basketItemDTO     = this.modelMapper.map(basketItem, BasketItemDTO.class);
            ProductDTO productDTO           = this.modelMapper.map(basketItem.getProduct(), ProductDTO.class);
            basketItemDTO.setProductDTO(productDTO);
            lBasketItemDTO.add(basketItemDTO);
        }
        basketDTO.setBasketItemDTO(lBasketItemDTO);
        return basketDTO;
    }
}

