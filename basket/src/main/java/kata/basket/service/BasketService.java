package kata.basket.service;

import kata.basket.dto.BasketDTO;
import kata.basket.model.Basket;

public interface BasketService {
    Basket create();
    BasketDTO findById(Long id);
    BasketDTO addItemToBasket(Long productId, int quantity);
    void removeItemFromBasketAuto();
    BasketDTO getCurrent();
}
