package kata.basket.service;

import kata.basket.dto.BasketItemDTO;
import kata.basket.exception.BasketItemNotFoundException;
import kata.basket.exception.ProductNotFoundException;
import kata.basket.exception.ProductQuantityAvailableException;
import kata.basket.model.BasketItem;
import kata.basket.model.Product;
import kata.basket.repository.BasketItemRepository;
import kata.basket.repository.ProductRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service
public class BasketItemServiceImpl implements BasketItemService {
    private final BasketItemRepository basketItemRepository;
    private final ProductRepository productRepository;
    private final ProductService productService;
    private final ModelMapper modelMapper;

    public BasketItemServiceImpl(BasketItemRepository basketItemRepository, ProductRepository productRepository, ProductService productService, ModelMapper modelMapper) {
        this.basketItemRepository   = basketItemRepository;
        this.productRepository      = productRepository;
        this.productService         = productService;
        this.modelMapper            = modelMapper;
    }

    @Override
    public BasketItemDTO updateItemToBasket(Long basketItemId, int quantity){
        BasketItem basketItem = basketItemRepository.findById(basketItemId).orElseThrow(() -> new BasketItemNotFoundException(basketItemId));
        Product product = productRepository.findById(basketItem.getProduct().getId()).orElseThrow(() -> new ProductNotFoundException(basketItem.getProduct().getId()));
        product.setQuantityAvailable(product.getQuantityAvailable() + basketItem.getQuantity());
        productRepository.save(product);
        if (product.getQuantityAvailable() > 0 && product.getQuantityAvailable() <= quantity)
            throw new ProductQuantityAvailableException(product.getId());
        basketItem.setQuantity(quantity);
        productService.decrementQuantityAvailable(basketItem.getProduct(), quantity);
        return this.modelMapper.map(basketItemRepository.save(basketItem), BasketItemDTO.class);
    }

    @Override
    public void removeBasketItem(Long basketItemId) {
        basketItemRepository.deleteById(basketItemId);
    }
}
