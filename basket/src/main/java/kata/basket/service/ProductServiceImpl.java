package kata.basket.service;

import kata.basket.dto.ProductDTO;
import kata.basket.exception.ProductNotFoundException;
import kata.basket.exception.QuantityInvalidException;
import kata.basket.model.Product;
import kata.basket.repository.ProductRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ProductServiceImpl implements ProductService {
    private final ProductRepository productRepository;
    private final ModelMapper modelMapper;
    public ProductServiceImpl(ProductRepository productRepository, ModelMapper modelMapper) {
        this.productRepository = productRepository;
        this.modelMapper    = modelMapper;
    }
    @Override
    public List<ProductDTO> findAll() {
        List<ProductDTO> products = productRepository.findAll().stream()
                .map(product -> modelMapper.map(product, ProductDTO.class))
                .collect(Collectors.toList());
        return products;
    }
    @Override
    public Product decrementQuantityAvailable(Product product, int quantity) {
        product.setQuantityAvailable(product.getQuantityAvailable() - quantity);
        if (product.getQuantityAvailable() < 0)
            throw new QuantityInvalidException();
        return productRepository.save(product);
    }
    @Override
    public ProductDTO findById(Long id) {
        Product product         = productRepository.findById(id).orElseThrow(() -> new ProductNotFoundException(id));
        ProductDTO productDTO   = modelMapper.map(product, ProductDTO.class);
        return  productDTO;
    }
}

