package kata.basket.service;

import kata.basket.dto.ProductDTO;
import kata.basket.model.Product;
import java.util.List;

public interface ProductService {
    List<ProductDTO> findAll();
    Product decrementQuantityAvailable (Product product, int quantity);
    ProductDTO findById(Long id);
}
