package kata.basket.service;

import kata.basket.dto.BasketItemDTO;

public interface BasketItemService {
    BasketItemDTO updateItemToBasket(Long basketItemId, int quantity);
    void removeBasketItem(Long basketItemId);
}