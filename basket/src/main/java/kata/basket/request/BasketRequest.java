package kata.basket.request;

import lombok.Data;

@Data
public class BasketRequest {
    private Long param1;
    private int param2;

}