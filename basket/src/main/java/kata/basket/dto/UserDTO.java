package kata.basket.dto;

import lombok.Data;

@Data
public class UserDTO {
    private Long id;
    private String name;
    private String firstname;
    private String login;
}
