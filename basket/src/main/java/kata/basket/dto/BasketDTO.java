package kata.basket.dto;

import lombok.Data;
import java.time.LocalDateTime;
import java.util.List;

@Data
public class BasketDTO {
    private Long id;
    private List<BasketItemDTO> basketItemDTO;
    private LocalDateTime createddate;
    private UserDTO user;
}