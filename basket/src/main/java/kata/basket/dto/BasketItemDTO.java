package kata.basket.dto;

import lombok.Data;

@Data
public class BasketItemDTO {
    private Long id;
    private ProductDTO productDTO;
    private int quantity;
}