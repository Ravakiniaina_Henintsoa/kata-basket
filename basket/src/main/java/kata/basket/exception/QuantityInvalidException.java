package kata.basket.exception;

public class QuantityInvalidException extends RuntimeException {
    public QuantityInvalidException() {
        super("Quantity invalid");
    }
}