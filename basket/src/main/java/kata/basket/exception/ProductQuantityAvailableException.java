package kata.basket.exception;

public class ProductQuantityAvailableException extends RuntimeException {
    public ProductQuantityAvailableException(Long id) {
        super("Quantity product " + id + " available");
    }
}
