package kata.basket.exception;

public class BasketItemNotFoundException extends RuntimeException {
    public BasketItemNotFoundException(Long id) {
        super("Could not find basket item" + id);
    }
}
