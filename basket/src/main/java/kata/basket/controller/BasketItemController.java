package kata.basket.controller;

import kata.basket.dto.BasketItemDTO;
import kata.basket.request.BasketItemRequest;
import kata.basket.service.BasketItemService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/basketItems")
public class BasketItemController {
    private final BasketItemService basketItemService;

    public BasketItemController(BasketItemService basketItemService) {
        this.basketItemService  = basketItemService;
    }
    @PutMapping("/{basketItemId}")
    public ResponseEntity<BasketItemDTO> updateItemToBasket(
            @PathVariable Long basketItemId,
            @RequestBody BasketItemRequest basketItemRequest) {
        int quantity = basketItemRequest.getParam1();
        BasketItemDTO basketItem = basketItemService.updateItemToBasket(basketItemId, quantity);
        return new ResponseEntity<>(basketItem, HttpStatus.OK);
    }
    @DeleteMapping("/{basketItemId}")
    public ResponseEntity<String> removeBasketItem(@PathVariable Long basketItemId) {
        basketItemService.removeBasketItem(basketItemId);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}

