package kata.basket.controller;

import kata.basket.exception.BasketNotFoundException;
import kata.basket.request.BasketRequest;
import kata.basket.service.BasketService;
import kata.basket.dto.BasketDTO;
import kata.basket.dto.BasketItemDTO;
import kata.basket.dto.ProductDTO;
import kata.basket.model.Basket;
import kata.basket.model.BasketItem;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/baskets")
public class BasketController {
    private final BasketService basketService;
    public BasketController(BasketService basketService) {
        this.basketService  = basketService;
    }
    @GetMapping("/")
    public ResponseEntity<BasketDTO> retrieveOne() {
        BasketDTO basketDTO = basketService.getCurrent();
        return new ResponseEntity<>(basketDTO, HttpStatus.OK);
    }
    @PostMapping(value = "/")
    public ResponseEntity<BasketDTO> addItemToBasket(
            @RequestBody BasketRequest basketRequest) {
        Long productId  = basketRequest.getParam1();
        int quantity    = basketRequest.getParam2();
        BasketDTO basket = basketService.addItemToBasket(productId, quantity);
        if (basket == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(basket, HttpStatus.CREATED);
    }
}

