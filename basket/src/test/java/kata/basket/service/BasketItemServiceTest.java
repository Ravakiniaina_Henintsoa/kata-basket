package kata.basket.service;

import kata.basket.dto.BasketItemDTO;
import static org.junit.Assert.*;
import kata.basket.model.BasketItem;
import kata.basket.model.Product;
import kata.basket.repository.BasketItemRepository;
import kata.basket.repository.ProductRepository;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.modelmapper.ModelMapper;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Optional;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.*;

@SpringBootTest
public class BasketItemServiceTest {
    @Mock
    private BasketItemRepository basketItemRepository;

    @Mock
    private ProductRepository productRepository;

    @Test
    void testUpdateItem() {
        Product product1        = new Product(1L, "Le Pacte des MarchOmbres", "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum", 20, "le_pacte_des_marchOmbres.png", 5);
        when(productRepository.findById(product1.getId())).thenReturn(Optional.of(product1));
        BasketItem basketItem   = new BasketItem(1L, product1, 2);
        BasketItem upBasketItem = new BasketItem(1L, product1, 3);
        when(basketItemRepository.findById(1L)).thenReturn(Optional.of(basketItem));
        when(basketItemRepository.save(basketItem)).thenReturn(upBasketItem);
        ProductServiceImpl productService = new ProductServiceImpl(productRepository, new ModelMapper());
        BasketItemServiceImpl basketItemService = new BasketItemServiceImpl(basketItemRepository, productRepository, productService, new ModelMapper());
        BasketItemDTO basketItemDTO = basketItemService.updateItemToBasket(basketItem.getId(), 3);
        assertNotNull(basketItemDTO);
        assertEquals(basketItem.getQuantity(), basketItemDTO.getQuantity());
        assertEquals(product1.getQuantityAvailable(), 4);
    }

    @Test
    void testRemove() {
        Long basketItemId = 1L;
        ProductServiceImpl productService = new ProductServiceImpl(productRepository, new ModelMapper());
        BasketItemServiceImpl basketItemService = new BasketItemServiceImpl(basketItemRepository, productRepository, productService, new ModelMapper());
        basketItemService.removeBasketItem(basketItemId);
        verify(basketItemRepository, times(1)).deleteById(basketItemId);
    }
}
