package kata.basket.service;


import kata.basket.dto.ProductDTO;
import kata.basket.model.Product;
import kata.basket.repository.ProductRepository;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.modelmapper.ModelMapper;
import org.springframework.boot.test.context.SpringBootTest;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@SpringBootTest
class ProductServiceTest {
    @Mock
    private ProductRepository productRepository;

    @Test
    void testFindAll() {
        Product product1 = new Product(1L, "Le Pacte des MarchOmbres", "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum", 20, "le_pacte_des_marchOmbres.png", 5);
        Product product2 = new Product(2L, "Le Seigneur des anneaux", "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum", 10, "le_seigneur_des_anneaux.png", 2);
        List<Product> products = Arrays.asList(product1, product2);
        when(productRepository.findAll()).thenReturn(products);
        ProductServiceImpl productService = new ProductServiceImpl(productRepository, new ModelMapper());
        List<ProductDTO> productList = productService.findAll();
        assertNotNull(productList);
        assertEquals(productList.size(), 2);
    }

    @Test
    void testDecrementQuantityAvailable() {
        Product product = new Product(1L, "Le Pacte des MarchOmbres", "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum", 20, "le_pacte_des_marchOmbres.png", 5);
        when(productRepository.save(product)).thenReturn(product);
        ProductServiceImpl productService = new ProductServiceImpl(productRepository, new ModelMapper());
        Product newProduct = productService.decrementQuantityAvailable(product, 2);
        assertNotNull(newProduct);
        assertEquals(newProduct.getQuantityAvailable(), 3);
    }

    @Test
    void testFindById() {
        Product product1 = new Product(1L, "Le Pacte des MarchOmbres", "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum", 20, "le_pacte_des_marchOmbres.png", 5);
        when(productRepository.findById(product1.getId())).thenReturn(Optional.of(product1));
        ProductServiceImpl productService = new ProductServiceImpl(productRepository, new ModelMapper());
        ProductDTO foundProduct = productService.findById(product1.getId());
        assertNotNull(foundProduct);
        assertEquals(product1.getId(), foundProduct.getId());
        assertEquals(product1.getName(), foundProduct.getName());
    }

}