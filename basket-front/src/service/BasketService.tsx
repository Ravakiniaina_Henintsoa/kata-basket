import axios from 'axios';
import React from 'react';
import { IProductItem } from '../component/ListCustom/ListCustom';
import { IBasket } from '../component/ListItem/ListItem';

export const URL_BASKET     = `http://localhost:8080/baskets/`;

export const addItemToBasket = async ({id, photo, name, price, description}: IProductItem) => {
    axios.post(URL_BASKET, {
        param1: id,
        param2: 1
    })
    .then((response) => {
        console.log(response);
    }).catch((error) => {
        alert(error.response.data);
    });
};

export const getData = async (setData: React.Dispatch<React.SetStateAction<IBasket | undefined>>) => {
    const { data } = await axios.get(URL_BASKET);
    setData(data);
};
