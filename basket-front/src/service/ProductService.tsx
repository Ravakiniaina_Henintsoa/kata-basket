import axios from 'axios';
import React from 'react';
import { IProductItem } from '../component/ListCustom/ListCustom';

export const URL_PRODUCT    = `http://localhost:8080/products/`;


export const getData = async (setData: React.Dispatch<React.SetStateAction<never[]>>) => {
    const { data } = await axios.get(URL_PRODUCT);
    setData(data);
};

