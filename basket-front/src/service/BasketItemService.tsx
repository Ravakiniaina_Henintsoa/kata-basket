import axios from 'axios';
import React from 'react';

export const URL_BASKET_ITEM     = `http://localhost:8080/basketItems/`;

export const updateItemToBasket = async (id: number, quantity: number) => {
    axios.put(URL_BASKET_ITEM + id, {
        param1: quantity
    })
    .then(response => {
        console.log(response);
        window.location.reload();
    })
    .catch(error => {
        alert(error.response.data);
    });
};

export const removeBasketItem = async (id: Number) => {
    axios.delete(URL_BASKET_ITEM + id)
    .then(response => {
        console.log(response);
        window.location.reload();
    })
    .catch(error => {
        console.log(error);
    });
};
