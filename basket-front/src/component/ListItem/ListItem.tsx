
import { IProductItem } from '../ListCustom/ListCustom';
import livre from '../../livre.svg';
import React, { FC, useEffect, useState } from "react";
import Icon from 'react-icons-kit';
import {plus} from 'react-icons-kit/icomoon/plus';
import {minus} from 'react-icons-kit/icomoon/minus';
import {bin} from 'react-icons-kit/icomoon/bin';
import { updateItemToBasket } from '../../service/BasketItemService';
import { removeBasketItem } from '../../service/BasketItemService';

export interface IBasket {
   id: number;
   basketItemDTO: IBasketItem[];
   createddate: Date;
}

interface IBasketItem {
   id: number;
   productDTO: IProductItem;
   quantity: number;
}

interface DetailsCustomProps {
   value: IBasket;
}

const ListItem : React.FC<DetailsCustomProps> = (props) => {
   const {value} = props;
   const [form, setForm] = useState({name: "", type: "", options: []});

   const handleDownQuantity = (id: number, quantity: number) => {
      if (quantity - 1 >= 0)
         updateItemToBasket(id, quantity - 1);
   }

   const handleUpQuantity = (id: number, quantity: number) => {
      updateItemToBasket(id, quantity + 1);
   }

   const handleDelete = (id: number) => {
      removeBasketItem(id);
   }

   return (
      <div className="container">
            <div className="heading">
               <h1>
               Panier
               </h1>
            </div>
            <div className="cart">
               <div className="table">
                  <div className="layout-inline row th">
                     <div className="col col-pro">Livres</div>
                     <div className="col col-price align-center "> 
                        Prix
                     </div>
                     <div className="col col-qty align-center">Quantité</div>
                  </div>
                  {
                     value.basketItemDTO.map(( {id, productDTO, quantity}: IBasketItem) => (
                        <div className="layout-inline row">
                           <div className="col col-pro layout-inline">
                              <img className="box-image" src={livre} alt="Logo"/>
                              <p>{productDTO.name}</p>
                           </div>
                           <div className="col col-price col-numeric align-center ">
                              <p>${productDTO.price}</p>
                           </div>
                           <div className="col col-qty layout-inline">
                              <Icon icon={minus} onClick={() =>handleDownQuantity(id, quantity)}/>
                              <input type="numeric" value={quantity} />
                              <Icon icon={plus} onClick={() =>handleUpQuantity(id, quantity)}/>
                           </div>
                           <div className="col col-action layout-inline"> 
                              <Icon icon={bin} onClick={() =>handleDelete(id)}/>
                           </div>
                        </div> 
                     ))
                  }
               </div>
            </div>
      </div>
   );
}

export default ListItem;