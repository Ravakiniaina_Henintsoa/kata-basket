import React, { FC, useEffect, useState, useContext  } from 'react';
import { useNavigate } from "react-router-dom";
import Icon from 'react-icons-kit';
import {plus} from 'react-icons-kit/icomoon/plus'
import livre from '../../livre.svg';
import { addItemToBasket } from '../../service/BasketService';

export interface IProductItem {
   name: string;
   description: string;
   photo: string;
   price: number;
   id: number;
}

interface IListCustomProps {
    value: IProductItem[];
}

const ListCustom : FC<IListCustomProps> = (props) => {
    const { value } = props;

    useEffect(() => {
        setData(value)
    }, [value]);

    const [data, setData] = useState<IProductItem[]>([]);

    const handleCart = (item: IProductItem) => {
        addItemToBasket(item);
    };

    return (
        <div>
           <div className="product-container">
                {
                    data.map(( {id, photo, name, price, description}: IProductItem) => (
                        <div className="product-card">
                            <div className="product-tumb">
                                <img className="box-image" src={livre} alt="Logo"/>
                            </div>
                            <div className="product-details">
                                <h4><a href="">{name}</a></h4>
                                <p>{description}</p>
                                <div className="product-bottom-details">
                                    <div className="product-price">${price}</div>
                                    <div className="product-links">
                                        <Icon icon={plus} onClick={() =>handleCart({id, photo, name, price, description})}/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    ))
                }
            </div>
        </div>
    );
}

export default ListCustom;