import React, { useEffect, useState } from 'react';
import { getData } from '../service/BasketService';
import ListItem from '../component/ListItem';
import { IBasket } from '../component/ListItem/ListItem';

const BasketPage = () => {

    const [data, setData] = useState<IBasket>();

    useEffect(() => {
        getData(setData);
    }, []);
    console.log(data)
    return (
        data ? <ListItem value = {data} /> : <></>
    );
}

export default BasketPage;