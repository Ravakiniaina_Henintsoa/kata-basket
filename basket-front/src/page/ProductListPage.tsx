import React, { useEffect, useState } from 'react';
import ListCustom from '../component/ListCustom';
import { getData } from '../service/ProductService';

const ProductListPage = () => {

    const [data, setData] = useState([]);

    useEffect(() => {
        getData(setData);
    }, []);
    
    return (
        <ListCustom value = {data} />
    );
}

export default ProductListPage;