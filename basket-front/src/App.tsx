import { Link, useLocation } from 'react-router-dom';
import './App.css';
import Router from './route/Router';
import { BrowserRouter } from "react-router-dom";
import React, { useEffect, useState } from 'react';
import Icon from 'react-icons-kit';
import {cart} from 'react-icons-kit/icomoon/cart'

function App() {

  const [active, setActive] = useState('');

  const handleClick = (value: string) => {
    setActive(value);
  };

  useEffect(() => {
    setActive(window.location.pathname); 
  }, []);

  return (
    <BrowserRouter>
        <ul className='menu'>
          <li>
            <Link className={`${active == '/' ? 'active' : ''}`} to="/" onClick={() =>handleClick('/')}>Accueil</Link>
          </li>
          <li>
            <Link className={`${active == '/basket' ? 'active' : ''}`} to="/basket" onClick={() =>handleClick('/basket')}><Icon icon={cart}/></Link>
          </li>
        </ul>
      <Router />
    </BrowserRouter>
  );
}

export default App;
