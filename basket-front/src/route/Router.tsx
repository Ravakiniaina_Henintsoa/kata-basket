import React, { useState } from 'react';
import { BrowserRouter, Routes, Route } from "react-router-dom";
import ProductListPage from '../page/ProductListPage';
import BasketPage from '../page/BasketPage';

const Router = () => {
    return (
        <div>
            <Routes>
                <Route path="/" element={<ProductListPage />} />
                <Route path="/basket" element={<BasketPage />} />
            </Routes>
        </div>   
    );
}

export default Router;